/*
 * van_leer_limiter_tests.c
 */


#include <cgreen/cgreen.h>
#include <math.h>
#include "./../src/van_leer_limiter.h"


#define MAXVAL (12.34)
#define randval ((double)rand() / (double)RAND_MAX * MAXVAL * pow(-1, rand()))


Describe(van_leer_limiter);
BeforeEach(van_leer_limiter) {};
AfterEach(van_leer_limiter) {};


Ensure(van_leer_limiter, )
{
  srand(12345);

  double dl, dr, r, phi_vl, alpha;

  hydro_t ql = {.vars = {0.0, 0.0, 0.0, 0.0, 0.0}};
  hydro_t q  = {.vars = {0.0, 0.0, 0.0, 0.0, 0.0}};
  hydro_t qr = {.vars = {0.0, 0.0, 0.0, 0.0, 0.0}};
  hydro_t dq = {.vars = {0.0, 0.0, 0.0, 0.0, 0.0}};

  for (int i = 0; i < 17; i++)
  {
    ql.rho = randval;
    q.rho = randval;
    qr.rho = randval;

    van_leer_liiter(&ql, &q, &qr, &dq);

    dl = q.rho - ql.rho;
    dr = qr.rho - q.rho;

    r = dr / dl;

    phi_vl = (r + fabs(r)) / (1.0 + fabs(r));

    alpha = dl * phi_vl;

    assert_that_double(dq.rho, is_equal_to_double(alpha));
  }
}
