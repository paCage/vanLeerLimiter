#ifndef _VAN_LEER_LIMITER_H_
#define _VAN_LEER_LIMITER_H_


#include <HydroBase.h>


#ifdef __cplusplus
extern "C" {
#endif

void van_leer_liiter(hydro_t *ql, hydro_t *q, hydro_t *qr, hydro_t *dq);

#ifdef __cplusplus
}
#endif


#endif /* _VAN_LEER_LIMITER_H_ */
