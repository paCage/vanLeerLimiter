/*
 * van_leer_limiter.c
 * tests_file: van_leer_limiter_tests.c
 *
 * Calculating van Leer slope limiter for a given state
 *
 * @param: ql: Hydro state at the left side
 * @param: q: Hydro state at the center
 * @param: qr: Hydro state at the right side
 * @param: dq: Calculated van Leer limiter for the center hydro state
 *
 * @return: void
 */


#include <HydroBase.h>
#include "./van_leer_limiter.h"

void van_leer_liiter(hydro_t *ql, hydro_t *q, hydro_t *qr, hydro_t *dq)
{
  double dl, dr;

  for (int i = rho_h; i <= p_h; i++)
  {
    dl = q->vars[i] - ql->vars[i];
    dr = qr->vars[i] - q->vars[i];

    dq->vars[i] = (dl * dr <= 0.0) ? 0.0 : 2.0 * dl * dr / (dl + dr);
  }
}
