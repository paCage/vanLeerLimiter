# van Leer Limiter
van Leer (1974) slope/flux limiter

# Description
In order to avoid new extrema in piecewise linear reconstruction, we need to
limit slopes, especially at shock position in a way that the reconstructed
values do not exceed or fall short of the origianl values. In another words,
slope/flux limiters are used in order to limit the soluton gradient near
shocks, discontinuities or sharp changes to avoid spurious oscillations.

In the general form, the slope at a given cell can be defined by using the
neighboring cells as follow:

```math
\alpha_i = \frac{u_i - u_{i -1}}{\Delta x} \phi(r),
r = \frac{u_{i+1} - u_i}{u_i - u_{i - 1}}
```

$`\phi(r)`$ is a nonlinear function since it has to be a second order TVD
(total vairation diminishing) and accordingly must satisfies as least the
following criteria:

```math
\begin{aligned}
r < 0 &\rightarrow \phi_{vl}(r) = 0 \\
0 \leq r \leq1 &\rightarrow  r \leq \phi_{vl} (r) \leq 2r \\
r = 1 &\rightarrow \phi_{vl} (r) = 1 \\
1 \leq r \leq 2 &\rightarrow  1 \leq \phi_{vl} (r) \leq r \\
r \geq 2 &\rightarrow  1 \leq \phi_{vl} (r) \leq 2
\end{aligned}
```

The above constraints, produce a valid region in the $`\phi(r)\text{-}r`$
diagram which is shown in the following figure,

![limiter-regions](assets/limiter-regions.png)

The van Leer slope/flux limiter, $`\phi_{vl}(r)`$, is defined as,

```math
\phi_{vl} (r) = \frac{r + |r|}{1 + |r|}
```

which is well within the viable region of $r\text{-}\phi(r)$ diagram.

Note that van Leer slope/flux limiter has the following symmetry,

```math
\frac{\phi_{vl} (r)}{r} = \phi_{vl} \left( \frac{1}{r} \right)
```

and

```math
\lim_{r\to\infty} \phi_{vl} (r) = 2
```

Using this new limited slope, now we can reconstruct the value of the state
variables (or fluxes) at the cell boundaries as follow,

```math
u_{i + \frac 1 2} = u_i + S_i \phi(r) \frac{\Delta x}{2}
```

where $`S_i`$ is the slope at cell $`i`$.
